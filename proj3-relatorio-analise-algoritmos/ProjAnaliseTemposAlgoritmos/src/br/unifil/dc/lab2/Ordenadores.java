package br.unifil.dc.lab2;

import java.util.ArrayList;
import java.util.List;

public class Ordenadores {

    /**
     * Ordena uma lista de inteiros usando o método da bolha.
     *
     * @param vals lista de inteiros, não nula.
     */
    public static void bubblesort(List<Integer> vals) {
        assert vals != null : "Lista não pode ser nula.";

        boolean houveTroca;
        do {
            houveTroca = false;
            // Varredura
            for (int i = 0; i < vals.size()-1; i++) {
                if (vals.get(i) > vals.get(i+1)) {
                    trocar(vals, i, i+1);
                    houveTroca = true;
                }
            }
        } while(houveTroca);
    }

    /**
     * Ordena uma lista de inteiros usando o método da seleção.
     *
     * @param vals lista de inteiros, não nula.
     */
    public static void selectionsort(List<Integer> vals) {
        assert vals != null : "Lista não pode ser nula.";

        for (int i = 0; i < vals.size()-1; i++) {
            // Qual o índice do menor elemento?
            int menor = i;
            for (int j = i+1; j < vals.size(); j++) {
                if (vals.get(menor) > vals.get(j))
                    menor = j;
            }

            trocar(vals, i, menor);
        }
    }

    /**
     * Ordena uma lista de inteiros usando o método da inserção.
     *
     * @param vals lista de inteiros, não nula.
     */
    public static void insertionsort(List<Integer> vals) {
        assert vals != null : "Lista não pode ser nula.";

        for (int i = 1; i < vals.size(); i++) {
            int elemAtual = vals.get(i);

            int posicaoTroca;
            for (posicaoTroca = i; posicaoTroca > 0; posicaoTroca--){
                if (vals.get(posicaoTroca-1) < elemAtual) break;
                vals.set(posicaoTroca, vals.get(posicaoTroca-1));
            }
            vals.set(posicaoTroca, elemAtual);
        }
    }

    /**
     * Ordena uma lista de inteiros usando o método da reunião.
     *
     * @param vals lista de inteiros, não nula.
     */
    public static void mergesort(List<Integer> vals) {
        assert vals != null : "Lista não pode ser nula.";
        mergesort(vals, 0, vals.size());
    }

    private static void mergesort(
            List<Integer> vals, int e, int d) {

        // Caso base
        final int numElems = d - e;
//        if (numElems <= 20) {
//            insertionsort(vals, e, d);
//            return;
//        }
        if (numElems <= 1) return;

        // Subdivisão
        final int meio = (e + d) / 2;
        mergesort(vals, e, meio);
        mergesort(vals, meio, d);
        merge(vals, e, d);
    }

    private static void merge(List<Integer> vals, int e, int d) {

        final int meio = (e + d) / 2;
        int topoEsq = e, topoDir = meio;
        ArrayList<Integer> intercalados = new ArrayList<>(d-e);

        while (topoEsq < meio && topoDir < d) {
            if (vals.get(topoEsq) < vals.get(topoDir)) {
                intercalados.add(vals.get(topoEsq));
                topoEsq++;
            } else {
                intercalados.add(vals.get(topoDir));
                topoDir++;
            }
        }

        while (topoEsq < meio) {
            intercalados.add(vals.get(topoEsq));
            topoEsq++;
        }
        while (topoDir < d) {
            intercalados.add(vals.get(topoDir));
            topoDir++;
        }

        for (int i = e, j = 0; i < d; i++, j++) {
            vals.set(i, intercalados.get(j)); // vals[i] = intercalados[j]
        }
    }

    /**
     * Ordena uma lista de inteiros usando o método da reunião.
     *
     * @param vals lista de inteiros, não nula.
     */
    public static void quicksort(List<Integer> vals) {
        quicksort(vals, 0, vals.size());
    }

    private static void quicksort(
            List<Integer> vals, int e, int d) {

        // Caso base
        if (d-e <= 1) {
            return;
        }

        // Subdivisão
        int idxPivo = escolherPivo(vals, e, d);
        idxPivo = particionar(vals, e, d, idxPivo);
        quicksort(vals, e, idxPivo);
        quicksort(vals, idxPivo+1, d);
    }

    /**
     * Recebe uma lista em vals, com sublista delimitada por 'e' e 'd'.
     * Ao retornar, entre 'e' e 'd', todos os elementos menores
     * que indicePivo estarão na esquerda, e todos os maiores estarão
     * na direita. A posição de indicePivo muda, e é retornada pelo
     * método.
     *
     * Utiliza o algoritmo original de Hoare.
     *
     * @param vals Referencia para a lista original que contém a
     *             sublista indicada pelos marcadores 'e' e 'd'.
     * @param e Índice do ínicio da sublista em vals.
     * @param d Índice do fim, fechado, da sublista em vals.
     * @param indicePivo Índice do elemento pré-escolhido como pivô.
     * @return Índice da nova posição de pivo, que permanece apontando
     * para o mesmo elemento pivo original.
     */
    public static int particionarHoare(
            List<Integer> vals, int e, int d, int indicePivo) {

        final Integer pivo = vals.get(indicePivo);
        trocar(vals, e, indicePivo);

        int menores = e, maiores = d-1;
        while (true) {
            while (vals.get(menores) < pivo) menores++;
            while (vals.get(maiores) > pivo) maiores--;
            if (menores >= maiores) return maiores;

            trocar(vals, menores, maiores);
        }
    }

    /**
     * Recebe uma lista em vals, com sublista delimitada por 'e' e 'd'.
     * Ao retornar, entre 'e' e 'd', todos os elementos menores
     * que indicePivo estarão na esquerda, e todos os maiores estarão
     * na direita. A posição de indicePivo muda, e é retornada pelo
     * método.
     *
     * Utiliza o algoritmo original de Lomuto.
     *
     * @param vals Referencia para a lista original que contém a
     *             sublista indicada pelos marcadores 'e' e 'd'.
     * @param e Índice do ínicio da sublista em vals.
     * @param d Índice do fim, fechado, da sublista em vals.
     * @param indicePivo Índice do elemento pré-escolhido como pivô.
     * @return Índice da nova posição de pivo, que permanece apontando
     * para o mesmo elemento pivo original.
     */
    public static int particionarLomuto(
            List<Integer> vals, int e, int d, int indicePivo) {

        final Integer pivo = vals.get(indicePivo);
        trocar(vals, d-1, indicePivo);

        int divisor = e - 1;
        for (int i = e; i < d-1; i++) {
            if (vals.get(i) < pivo) {
                divisor++;
                trocar(vals, divisor, i);
            }
        }
        trocar(vals, divisor+1, d-1);
        return divisor + 1;
    }

    private static int particionar(
            List<Integer> vals, int e, int d, int indicePivo) {

        // Escolha seu método, comentando uma das linhas
        // e descomentando a outra.
        return particionarHoare(vals, e, d, indicePivo);
        //return particionarLomuto(vals, e, d, indicePivo);
    }

    private static int escolherPivo(List<Integer> vals, int e, int d) {
        return d-1;
    }

    private static void trocar(List<Integer> vals, int i, int j) {
        Integer aux = vals.get(i);
        vals.set(i, vals.get(j));   // vals[i] = vals[j]
        vals.set(j, aux);           // vals[j] = aux
    }
}
