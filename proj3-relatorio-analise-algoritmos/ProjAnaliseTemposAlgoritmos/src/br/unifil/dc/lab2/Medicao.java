package br.unifil.dc.lab2;

public class Medicao {
    public Medicao(int dominioAmostra, long tempoNano) {
        this(dominioAmostra, tempoNano * 1e9);
    }

    public Medicao(int dominioAmostra, double tempoSegundos) {
        this.dominioAmostra = dominioAmostra;
        this.tempoSegundos = tempoSegundos;
    }

    public int getDominioAmostra() {
        return dominioAmostra;
    }

    public double getTempoSegundos() {
        return tempoSegundos;
    }

    private int dominioAmostra;
    private double tempoSegundos;
}
