package br.unifil.dc.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Benchmarking {

    public static List<Medicao> benchmarkIntervalo(
            int inicial, int limite, int passo, int repeticoes,
            Consumer<List<Integer>> metodo) {

        ArrayList<Medicao> tempos = new ArrayList<>();
        for (; inicial < limite; inicial+=passo) {
            List<Integer> listaA = revRange(0,inicial)
                    .boxed()
                    .collect(Collectors.toList());

            tempos.add(new Medicao(
                    inicial, benchmarkLista(listaA,repeticoes, metodo)));
        }

        return tempos;
    }

    public static double benchmarkLista(
            List<Integer> listaOriginal,
            int repeticoes,
            Consumer<List<Integer>> metodo) {

        return IntStream.range(0, repeticoes)
            .mapToLong((i) -> {
                List<Integer> listaCopiada
                        = new ArrayList<>(listaOriginal);

                // Anotar tempo atual
                final long tempoAnterior = System.nanoTime();

                // Realizar tarefa
                metodo.accept(listaCopiada);

                // Anotar tempo pós-tarefa
                final long tempoPosterior = System.nanoTime();
                return tempoPosterior - tempoAnterior;
            })
            .average().getAsDouble();
    }

    private static long calcularMedia(List<Long> valores) {
        long accumulador = 0;
        for (Long v : valores) accumulador += v;
        return accumulador / valores.size();
    }


    // Código de: https://stackoverflow.com/questions/24010109/java-8-stream-reverse-order#24011264
    private static IntStream revRange(int from, int to) {
        return IntStream.range(from, to)
                .map(i -> to - i + from);
    }
}
