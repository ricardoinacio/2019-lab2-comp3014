package br.unifil.dc.sisop.Ordenadores;

import java.util.Comparator;
import java.util.List;

public class Quicksorter {

    /**
     * Classifica os elementos de vals, in-place, pelo algoritmo de quicksort,
     * de acordo com a ordem natural de T.
     *
     * @param vals lista com valores a serem classificados.
     */
    public static <T extends Comparable<T>> void quicksort(List<T> vals) {
        quicksort(vals, (a,b) -> b.compareTo(a));
    }

    /**
     * Classifica os elementos de vals guiado pela função c, in-place, pelo
     * algoritmo de quicksort.
     *
     * @param vals lista com valores a serem classificados.
     * @param c função de classificação.
     */
    public static <T> void quicksort(List<T> vals, Comparator<T> c) {
        quicksort(vals, 0, vals.size(), c);
    }

    private static <T> void quicksort(List<T> vals, int l, int r,
                                      Comparator<T> c) {
        assert r >= l;
        assert r <= vals.size();

        // Caso base: há uma ou menos cartas entre l e r
        if (r-l <= 1) return;

        // Divisão e conquista
        final int m = (l+r)/2;
        final int idxPivo = r-1;
        final int novaIdxPivo = particionar(vals, idxPivo, l, r, c); // (O(n), Omega(log_2 n)) * Theta(n)

        quicksort(vals, l, novaIdxPivo, c);
        quicksort(vals, novaIdxPivo, r, c);
    } // O(n^2), Omega(n * log_2 n)

    /**
     * Particiona o subdominio de vals de l a p como menores que
     * vals[p], e de p a r como como maiores que vals[p].
     *
     * @param vals Lista contendo subdominio l <= p <= r.
     * @param p Índice do pivo em vals.
     * @param l Índice inicial de vals.
     * @param r Índice final de vals.
     * @param c Função de classificação.
     * @return posição final do pivo, após o particionamento in-place.
     */
    private static <T> int particionar(List<T> vals,
                                       int p, int l, int r, Comparator<T> c){
        assert r >= l;
        assert r >= p && p >= l;
        assert r <= vals.size();

        throw new RuntimeException("O aluno ainda não implementou.");
    }
}
