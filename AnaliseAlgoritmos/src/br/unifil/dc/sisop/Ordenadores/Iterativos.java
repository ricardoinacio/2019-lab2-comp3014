package br.unifil.dc.sisop.Ordenadores;

import br.unifil.dc.sisop.Uteis;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Iterativos {

    /**
     * Ordena crescentemente elems pelo algoritmo da inserção,
     * in-place.
     *
     * ATENCAO: O método modifica elems!
     *
     * @param elems lista a ser ordenada in-place, mutável.
     */
    public static <T> void ordenarInsercao(List<T> elems, Comparator<T> cmp) {
        for (int i = 1; i < elems.size(); i++) { // (n)
            final T comprada = elems.get(i);
            for (int j = i-1; j >= 0; j--) { // (n) * (n) = (n^2)
                final T topoMao = elems.get(j);
                if (cmp.compare(topoMao, comprada) > 0) {
                    elems.set(j+1, topoMao);
                    elems.set(j, comprada);
                } else {
                    break;
                }
            }
        }
    } // O(n^2), Ω(n), não existe theta

    /**
     * Implementação definitiva para ordenação pelo método da bolha,
     * in-place.
     *
     * @param elems lista de elementos a ser ordenada.
     * @param cmp função de comparação entre T's.
     * @param <T> Tipo de dados da lista, que serão comparados.
     */
    public static <T> void ordenarBolha(List<T> elems, Comparator<T> cmp) {
        boolean houvePermuta;
        do {
            houvePermuta = false; // n, 1
            for (int i = 1; i < elems.size(); i++) { // n^2, n
                final T esq = elems.get(i-1); // n*(n-1)*1 = n^2-n, n-1
                final T dir = elems.get(i); // n*(n-1)*1 = n^2-n, n-1
                if (cmp.compare(esq,dir) > 0) { // n*(n-1)*1 = n^2-n, n-1
                    Uteis.permutar(elems, i-1, i); // 3*n*(n-1)*1 = 3n^2-3n, 0
                    houvePermuta = true; // n*(n-1)*1 = n^2-n, 0
                }
            }
        } while (houvePermuta); // pior: n+1, melhor: 1
    } // elems totalmente desordenado, n: elems.size(),
    // T_pior(n) = 8n^2 - 5n + 1 -> (n^2), T_melhor(n) = 4n - 1 -> (n)
    public static <T extends Comparable<T>> void ordenarBolha(List<T> elems) {
        ordenarBolha(elems, (a,b) -> a.compareTo(b));
    }

    /**
     * Ordena crescentemente elems pelo algoritmo da seleção,
     * in-place.
     *
     * ATENCAO: O método modifica elems!
     *
     * @param elems lista a ser ordenada in-place, mutável.
     */
    public static <T> void ordenarSelecao(List<T> elems, Comparator<T> cmp) {
        // O menor item para a posição i
        for (int i = 0; i < elems.size()-1; i++) { // n
            int idxMenor = getIdxMenor(elems, i, cmp); // (n-1) * (5n-2) = 5n^2 - 5n - 2n + 2
            Uteis.permutar(elems, i, idxMenor); // (n-1) * 3 = 3n-3
        }
    } // n: elems.size(), T(n) = n + 5n^2 - 5n - 2n + 2 + 3n-3
    // T_pior(n) = 5n^2 - 3n - 1, T_melhor(n) = 4n^2 - 2n - 2

    public static <T extends Comparable<T>> void ordenarSelecao(List<T> elems) {
        ordenarSelecao(elems, (a,b) -> a.compareTo(b)); // 1 * T_ordSel() = 5n^2 - 3n - 1
    } // n: elems.size(), T(n) = 5n^2 - 3n - 1

    private static <T> int getIdxMenor(List<T> elems, int i, Comparator<T> cmp) {
        int idxMenor = i; // 1
        for (int j = i+1; j < elems.size(); j++) { // n
            final T proxElem = elems.get(j); // n-1
            final T menorAtual = elems.get(idxMenor); // n-1
            if (cmp.compare(menorAtual, proxElem) > 0) { // n-1
                idxMenor = j; // pior: n-1, melhor: 0
            }
        }
        return idxMenor; // 1
    } // lista totalmente desordenada, n: elems.size(),
    // T_pior(n) = 5n - 2, T_melhor(n) = 4n - 1







    private static <T> void _ordenarSelecao(List<T> elems, Comparator<T> cmp) {
        // O menor item para a posição i
        for (int i = 0; i < elems.size()-1; i++) { // (n)
            int idxMenor = _getIdxMenor(elems, i, cmp); // (n) * _getIdxMenor = (n) * (n) = (n^2)
            Uteis.permutar(elems, i, idxMenor); // (n) * (1) = (1*n) = (n)
        }
    } // n: elems.size(), (n^2)


    private static <T extends Comparable<T>> void _ordenarSelecao(List<T> elems) {
        _ordenarSelecao(elems, (a,b) -> a.compareTo(b)); // (1) * (n^2)
    } // Para qualquer caso, n: elems.size(), (n^2)

    private static <T> int _getIdxMenor(List<T> elems, int i, Comparator<T> cmp) {
        int idxMenor = i; // (1)
        for (int j = i+1; j < elems.size(); j++) { // (n)
            final T proxElem = elems.get(j); // (n)
            final T menorAtual = elems.get(idxMenor); // (n)
            if (cmp.compare(menorAtual, proxElem) > 0) { // (n)
                idxMenor = j; // (n)
            }
        }
        return idxMenor; // (1)
    } // Pior, n: elems.size() total desordenado,  (5n + 2) = (n)
    // Melhor, n: elems.size() total ordenado,  (4n + 2) = (n)



    public static void testarOrdenadoresIterativos() {
        Comparator<Integer> intCmp = (l, r) -> l.compareTo(r);

        System.out.println("Seleção:");
        List<Integer> vals = new ArrayList<>(
                List.of(7,5,10,12,14,0,-5));
        Iterativos.ordenarSelecao(vals, intCmp);
        System.out.println(vals.toString());

        vals = new ArrayList<>(
                List.of(0,1,2,3,4,5,6));
        Iterativos.ordenarSelecao(vals, intCmp);
        System.out.println(vals.toString());


        System.out.println("Bolha:");
        vals = new ArrayList<>(
                List.of(7,5,10,12,14,0,-5));
        Iterativos.ordenarBolha(vals, intCmp);
        System.out.println(vals.toString());

        vals = new ArrayList<>(
                List.of(0,1,2,3,4,5,6));
        Iterativos.ordenarBolha(vals, intCmp);
        System.out.println(vals.toString());


        System.out.println("Bolha decrescente:");
        vals = new ArrayList<>(
                List.of(7,5,10,12,14,0,-5));
        Iterativos.ordenarBolha(vals, (l, r) -> -1 * intCmp.compare(l,r));
        System.out.println(vals.toString());

        vals = new ArrayList<>(
                List.of(0,1,2,3,4,5,6));
        Iterativos.ordenarBolha(vals, (l, r) -> -1 * intCmp.compare(l,r));
        System.out.println(vals.toString());


        System.out.println("Inserção:");
        vals = new ArrayList<>(
                List.of(7,5,10,12,14,0,-5));
        Iterativos.ordenarInsercao(vals, intCmp);
        System.out.println(vals.toString());

        vals = new ArrayList<>(
                List.of(0,1,2,3,4,5,6));
        Iterativos.ordenarInsercao(vals, intCmp);
        System.out.println(vals.toString());
    }
}
