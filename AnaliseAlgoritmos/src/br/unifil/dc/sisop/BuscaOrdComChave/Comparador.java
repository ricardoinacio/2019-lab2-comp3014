package br.unifil.dc.sisop.BuscaOrdComChave;


/**
 * Melhor implementação.
 */
public interface Comparador<T> {

    /**
     * Compara o inteiro da esquerda com o da direita.
     *
     * @param l inteiro da esquerda.
     * @param d inteiro da direita.
     * @return 0 se forem iguais, -1 se o inteiro da esquerda
     * for menor, e 1 se o mesmo for maior.
     */
    int comparar(T l, T d);
}

/**
 * DON'T
 */
interface ComparadorSmelly {

    /**
     * Compara o inteiro da esquerda com o da direita.
     *
     * @param l inteiro da esquerda.
     * @param d inteiro da direita.
     * @return 0 se forem iguais, -1 se o inteiro da esquerda
     * for menor, e 1 se o mesmo for maior.
     */
    int comparar(Object l, Object d);
}
