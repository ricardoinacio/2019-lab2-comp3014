package br.unifil.dc.sisop.BuscaOrdComChave;

import br.unifil.dc.sisop.Ordenadores.Iterativos;

import java.util.ArrayList;
import java.util.List;

public class Aluno {

    public static void testarOrdenacaoChaveDeBusca() {

        System.out.println("Ordenação por chaves (atributos) variáveis:");
        List<Aluno> classe = new ArrayList<>(List.of(
                new Aluno("Thierris", 29, Aluno.EstadoSaude.CANSADO, "000000"),
                new Aluno("Braguini", 19, Aluno.EstadoSaude.ESTAGIARIO, "222222"),
                new Aluno("Zamboni", 21, Aluno.EstadoSaude.DOENTE, "111111"),
                new Aluno("Natalia", 19, Aluno.EstadoSaude.SAUDAVEL, "333333")));

        Iterativos.ordenarBolha(classe, (l, r) -> l.getNome().compareTo(r.getNome()));
        System.out.println(classe.toString());
        Iterativos.ordenarBolha(classe, (l, r) -> l.getMatricula().compareTo(r.getMatricula()));
        System.out.println(classe.toString());
    }

    public enum EstadoSaude {
        SAUDAVEL,
        CANSADO,
        POBRE,
        ESTAGIARIO,
        DOENTE
    }

    public Aluno(String nome, int idade, EstadoSaude estado, String matricula) {
        this.nome = nome;
        this.idade = idade;
        this.estado = estado;
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public EstadoSaude getEstado() {
        return estado;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public String toString() {
        return nome + ": " + matricula;
    }

    private String nome;
    private int idade;
    private EstadoSaude estado;
    private String matricula;
}
