package Models;

import java.util.Date;

public class Aluno {

    private String nome;
    private String matricula;
    private Date ingresso;
    private Turma turmaCorrente = null;

    public Aluno(String nome, String matricula, Date ingresso) {
        this.nome = nome;
        this.matricula = matricula;
        this.ingresso = ingresso;
    }

    public String getNome() { return nome; }

    public String getMatricula() { return matricula; }

    public Date getIngresso() { return ingresso; }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Aluno) { // Só use instanceof em métodos equals. Smelly code!!!!
            Aluno alunoOutro = (Aluno) other;
            return this.nome.equals(alunoOutro.nome) &&
                this.matricula.equals((alunoOutro.matricula)) &&
                this.ingresso.equals((alunoOutro.ingresso));
        }
        return false;
    }
}
