package Filas;

public interface Fila<T> {
    void enfileirar(T elem);
    T desenfileirar();
    T proximoElem();
    T ultimoElem();
    int qtdeElems();
}
