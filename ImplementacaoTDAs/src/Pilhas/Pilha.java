package Pilhas;

public interface Pilha<T> {
    void empilhar(T elem);
    T desempilhar();
    T olharTopo();
    int qtdeElems();
}

class PilhaVaziaDesempilhadaException extends RuntimeException {
    PilhaVaziaDesempilhadaException(String msg) {
        super(msg);
    }
}
