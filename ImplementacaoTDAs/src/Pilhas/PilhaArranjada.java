package Pilhas;

public class PilhaArranjada<T> implements Pilha<T> {

    private T[] elems;
    private int topo;

    public PilhaArranjada() {
        elems = (T[]) new Object[10];
        topo = 0;
    }

    @Override
    public void empilhar(T elem) {
        if (elems.length == topo) this.expandirArranjo();

        elems[topo] = elem;
        topo++;
    } // O(n) quando expandir a pilha. Para todos os outros casos, O(1).

    @Override
    public T desempilhar() {
        topo--;
        T desempilhado = elems[topo];
        elems[topo] = null;

        return desempilhado;
    } // O(1)

    @Override
    public T olharTopo() {
        return elems[topo-1];
    } // O(1)

    @Override
    public int qtdeElems() {
        return topo;
    } // O(1)

    private void expandirArranjo() {
        T[] expandido = (T[]) new Object[Math.round(elems.length * 1.4f)];
        for (int i = 0; i < elems.length; i++) expandido[i] = elems[i];
        elems = expandido;
    }
}
