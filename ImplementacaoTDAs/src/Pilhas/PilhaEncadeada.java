package Pilhas;

class PilhaEncadeada<T> implements Pilha<T> {

    public PilhaEncadeada() {
        topo = null;
        qtdeElems = 0;
    }

    @Override
    public void empilhar(T elem) {
        No novoTopo = new No(elem);
        novoTopo.proximo = topo.proximo;
        topo = novoTopo;

        qtdeElems++;
    } // O(1)

    @Override
    public T desempilhar() {
        if (qtdeElems > 0) {
            qtdeElems--;

            T valor = topo.valor;
            topo = topo.proximo;
            return valor;
        }
        throw new PilhaVaziaDesempilhadaException("É isso aí, se f...");
    } // O(1)

    @Override
    public T olharTopo() {
        if (qtdeElems > 0) return topo.valor;

        throw new PilhaVaziaDesempilhadaException("É isso aí, se f...");
    } // O(1)

    @Override
    public int qtdeElems() {
        return qtdeElems;
    } // O(1)



    private int qtdeElems;
    private No topo;

    private class No {
        No proximo = null;
        T valor;

        No(T valor) { this.valor = valor; }
    }
}
