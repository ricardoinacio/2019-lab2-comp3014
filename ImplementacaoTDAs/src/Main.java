public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");
    }


    public static Par<Integer, Integer> divMod(int divisor, int dividendo) {
        return new Par<Integer, Integer>(
                Integer.valueOf(divisor / dividendo),
                Integer.valueOf(divisor % dividendo));
    }
}
