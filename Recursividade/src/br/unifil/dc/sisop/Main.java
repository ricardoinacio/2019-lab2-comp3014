package br.unifil.dc.sisop;

import java.util.Collections;
import java.util.List;

public class Main {

    /**
     * Soma os valores da lista de inteiros. Se não houver
     * valores, o resultado é 0.
     *
     * @param vals Lista de inteiros com valores a serem somados.
     * @return Soma dos valores da lista ou 0 se não houver
     * valores.
     */
    public static int somaRec(List<Integer> vals) {
        // Testando casos base
        if (vals.size() == 0) return 0;
        // if (vals.size() == 1) return vals.get(0); desnecessário

        // Computa subdivisões do problema
        int elemAtual = vals.get(0);
        List<Integer> restanteDaLista = vals.subList(1,vals.size());
        return  elemAtual + somaRec(restanteDaLista);
    }

    /**
     * Soma os valores da lista de inteiros. Se não houver
     * valores, o resultado é 0.
     *
     * @param vals Lista de inteiros com valores a serem somados.
     * @return Soma dos valores da lista ou 0 se não houver
     * valores.
     */
    public static int somaRecTail(List<Integer> vals) {
        return somaRecTail(vals, 0);
    }
    private static int somaRecTail(List<Integer> vals, int i) {
        // Testando casos base
        if (i >= vals.size()) return 0;

        // Computa subdivisões do problema
        int elemAtual = vals.get(i);
        return  elemAtual + somaRecTail(vals, i+1);
    }

    /**
     * Soma os valores da lista de inteiros. Se não houver
     * valores, o resultado é 0.
     *
     * @param vals Lista de inteiros com valores a serem somados.
     * @return Soma dos valores da lista ou 0 se não houver
     * valores.
     */
    public static int soma(List<Integer> vals) {
        int soma = 0;

//        for (int i = 0; i < vals.size(); i++) {
//            soma += vals.get(i); //soma += vals[i];
//        }
        for (int val : vals) {
            soma += val;
        }

        return soma;
    }

    /**
     * Soma os valores do arranjo. Se não houver valores,
     * o resultado é 0.
     *
     * @param vals Arranjo com valores a serem somados.
     * @return Soma dos valores do arranjo, ou 0 se não houver
     * valores.
     */
    public static int soma(int[] vals) {
        int soma = 0;

//        for (int i = 0; i < vals.length; i++) {
//            soma += vals[i];
//        }
        for (int val : vals) {
            soma += val;
        }

        return soma;
    }

    /**
     * Calcula o enésimo elemento da sequência de Fibonacci.
     * Limitação: apenas números positivos ou zero.
     *
     * @param n Número do elemento da sequencia a ser calculado.
     *          Deve ser igual ou maior que zero.
     * @return O enésimo elemento da sequencia de Fibonacci.
     */
    public static int fibonacciRec(int n) {
        assert n >= 0 : "Valores de n menores que zero não são aceitos.";

        // Casos base
        if (n == 0) return 0;
        if (n == 1) return 1;

        // Subdivisão recursiva
        return fibSoma(n);
    }

    public static int fibSoma(int n) {
        return fibonacciRec(n-1) + fibonacciRec(n-2);
    }

    /**
     * Calcula o enésimo elemento da sequência de Fibonacci.
     * Limitação: apenas números positivos ou zero.
     *
     * @param n Número do elemento da sequencia a ser calculado.
     *          Deve ser igual ou maior que zero.
     * @return O enésimo elemento da sequencia de Fibonacci.
     */
    public static int fibonacci(int n) {

        int fib0 = 0;
        int fib1 = 1;

        if (n < 2) return n;

        for (int i = 1; i < n; i++) {
            int proximoFib = fib0 + fib1;
            fib0 = fib1;
            fib1 = proximoFib;
        }

        return fib1;
    }

    private static String subStringAposSegundo(String s, char c) {
        for (int i = 2; i > 0; i--) {
            s = s.substring(s.indexOf(c)+1);
        }
        return s;
    }

    // Conhecido como método "helper"!
    public static String subStringAposSegundoRecursivo(String s, char c) {
        return subStringAposSegundoRecursivo(s, c, 2);
    }

    private static String subStringAposSegundoRecursivo(String s, char c, int i) {
        // Caso base
        if (i == 0) return s;

        // Caso recursivo (subdivisão, computação)
        s = s.substring(s.indexOf(c)+1);
        return subStringAposSegundoRecursivo(s, c, i-1);
    }

    public static void main(String[] args) {

        System.out.println(fibonacciRec(-10));

//        System.out.println(
//                subStringAposSegundoRecursivo("Olá, mundo, cruel!", ',')
//                        .equals(" cruel!"));

//        System.out.println(somaRecTail(List.of(1,2,3,4,5)));
//        System.out.println(somaRecTail(Collections.emptyList()));

    }
}
